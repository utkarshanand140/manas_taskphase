



import pandas as pd
import numpy as np
import matplotlib.pyplot as plt





temp = pd.read_csv('car_price.csv')






temp.columns





data = temp[['year', 'selling_price', 'km_driven', 'fuel', 'seller_type',
       'transmission', 'owner', 'mileage', 'engine', 'max_power', 'torque',
       'seats'
            ]]





data['Current_Year'] = 2020






data.head()


# In[8]:


data['# Years'] = data['Current_Year'] - data['year']


# In[9]:


to_drop = ['Current_Year','year','torque','max_power','seller_type','owner']
data.drop(to_drop, inplace = True, axis = 1)


# In[10]:



data.head()


# In[11]:


data['engine']= data['engine'].str.replace('[^\d.]', '',regex = True).astype(float)


# In[12]:



data['mileage'] = data['mileage'].str.replace('[^\d.]', '',regex = True).astype(float)


# In[13]:


data.head()


# In[14]:


data.replace(to_replace = ['Diesel','Petrol','LPG','CNG'],value=[1,2,3,4],inplace = True)


# In[15]:


data.head()


# In[16]:


data.replace(to_replace = ['Manual','Automatic'],value=[1.0,2.0],inplace = True)


# In[17]:


data.head()


# In[18]:


data.head()


# In[ ]:





# In[21]:


data = (data - data.mean())/data.std()



X = data.iloc[:,1:8]

ones = np.ones([X.shape[0],1])
X = np.concatenate((ones,X),axis=1)

y = data.iloc[:,0:1].values 
theta = np.zeros([1,8])

print(X)





def computeCost(X,y,theta):
    tobesummed = np.power(((X @ theta.T)-y),2)
    return np.sum(tobesummed)/(2 * len(X))

def gradientDescent(X,y,theta,iters,alpha):
    cost = np.zeros(iters)
    for i in range(iters):
        theta = theta - (alpha/len(X)) * np.sum(X * (X @ theta.T - y), axis=0)
        cost[i] = computeCost(X, y, theta)
        
    
    return theta,cost


alpha = 0.01
iters = 1000

g,cost = gradientDescent(X,y,theta,iters,alpha)
print(g)

finalCost = computeCost(X,y,g)
print(finalCost)


 
fig, ax = plt.subplots()  
ax.plot(np.arange(iters), cost, 'r')  
ax.set_xlabel('Iterations')  
ax.set_ylabel('Cost')  
ax.set_title('Error vs. Training Epoch')  


# In[ ]:




